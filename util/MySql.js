'use strict';

var config = require('../config/config');

var mysql = require('mysql');

module.exports = (function MySqlPool() {
    
    var MySqlPool = function (kraken) {
        var base = this;
	
		var pool = mysql.createPool({
			connectionLimit: config.db.connectionLimit || 30,
			host     : config.db.host,
			user     : config.db.user,
			password : config.db.password,
            database : config.db.name,
            timezone : "utc"
        });

        pool.on('connection', function (connection) {
            connection.query('set time_zone =\'Europe/London\';');
        });

        this.pool = pool;

    };

	return MySqlPool;

})();
