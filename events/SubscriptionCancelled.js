'use strict';

module.exports = (function() {

	function SubscriptionCancelled(message) {
		var data = JSON.parse(message.data);

		this.eventSerieId = data.event_serie;
	}
	
	return SubscriptionCancelled;

})();