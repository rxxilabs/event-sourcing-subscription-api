'use strict';

module.exports = (function() {

	function EmailRegistered(message) {
		
		var data = JSON.parse(message.data);
		
		this.id = message.aggId;
		this.email = data.email;
	
	}
	
	return EmailRegistered;

})();