'use strict';

module.exports = (function() {

	function SubscriptionConfirmed(message) {
		var data = JSON.parse(message.data);

		this.eventSerieId = data.event_serie;
	
	}
	
	return SubscriptionConfirmed;

})();