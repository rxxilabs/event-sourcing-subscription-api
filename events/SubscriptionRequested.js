'use strict';

module.exports = (function() {

	function SubscriptionRequested(message) {
		
		var data = JSON.parse(message.data);

		this.eventSerieId = data.event_serie;
	}
	
	return SubscriptionRequested;

})();