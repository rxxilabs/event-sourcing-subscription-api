'use strict';

module.exports = (function() {
	
	var boot;

	function Subscriber(_boot) {
		
		if (typeof _boot === 'undefined')
			throw new Error('_boot is not set');
		
		boot = _boot;
		
		this.id = '';
		this.email = '';
		this.confirmedSubscriptions = [];
		this.unconfirmedSubscriptions = [];
		this.cancelledSubscriptions = [];
	}
	
	Subscriber.prototype.Handle = function(Event) {
		//console.log("n", Event.constructor.toString(), Event instanceof boot.events.EmailRegistered);
		
		
		if (Event instanceof boot.events.EmailRegistered) {
			this.id = Event.id;
			this.email = Event.email;
		} 
		
		else if (Event instanceof boot.events.SubscriptionRequested) {
			this.unconfirmedSubscriptions.push(Event.eventSerieId);
		} 
		
		else if (Event instanceof boot.events.SubscriptionConfirmed) {
			
			var index = this.unconfirmedSubscriptions.indexOf(Event.eventSerieId);
			if (index > -1) {
				this.unconfirmedSubscriptions.splice(index, 1);
			}
			this.confirmedSubscriptions.push(Event.eventSerieId);
			
		} else if (Event instanceof boot.events.SubscriptionCancelled) {

			var index = this.confirmedSubscriptions.indexOf(Event.eventSerieId);
			if (index > -1) {
				this.confirmedSubscriptions.splice(index, 1);
			}
			this.cancelledSubscriptions.push(Event.eventSerieId);
			
		}
		
	}
	
	return Subscriber;

})();