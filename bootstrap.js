'use strict';

var shortid 				= require('shortid');

var Subscribe 				= require('./commands/Subscribe');
var EventSerie 				= require('./domains/EventSerie');
var Subscriber 				= require('./domains/Subscriber');
var EmailRegistered 		= require('./events/EmailRegistered');
var SubscriptionRequested 	= require('./events/SubscriptionRequested');
var SubscriptionConfirmed 	= require('./events/SubscriptionConfirmed');
var SubscriptionCancelled 	= require('./events/SubscriptionCancelled');
var Projections 			= require('./repositories/Projections');
var ProjectionHandlers 		= require('./queryHandlers/ProjectionHandlers');
var Promise 				= require("bluebird");
var multiline 				= require("multiline");
var _ 						= require('lodash-node');

module.exports = {
	commands: {
		Subscribe: Subscribe
	},
	events: {
		EmailRegistered: EmailRegistered,
		SubscriptionRequested: SubscriptionRequested,
		SubscriptionConfirmed: SubscriptionConfirmed,
		SubscriptionCancelled: SubscriptionCancelled
	},
	domains: {
		EventSerie: EventSerie,
		Subscriber: Subscriber
	},
	repos: {
		Projections: Projections
	},
	queryHandlers: {
		Projections: ProjectionHandlers
	},
	shortid: shortid,
	Promise: Promise,
	multiline: multiline,
	_: _
}