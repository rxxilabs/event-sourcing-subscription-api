'use strict';

module.exports = (function() {

	function Projections(boot) {
		this.boot = boot;
		this.pool = boot.db.mysql.pool;
	}
	
	Projections.prototype.getAllEvents = function() {
		var base = this;
		
		return new this.boot.Promise(function (resolve, reject) {
			base.pool.getConnection(function(err, connection) {
				if (err) return reject(new Error(err));
				
				var query = base.boot.multiline(function() {
					/*
						select
							e.aggregateId as aggId,
							e.`event`,
							e.`data`,
							e.`version`,
							e.date_created
						from
							eventstore e
						left join
							aggregates a
								on a.id = e.aggregateId
					*/
				});
				
				connection.query(query, [
				
				], function(err, rows, fields) {
					connection.release();
					
					if (err) return reject(new Error(err));
					
					return resolve({ rows: rows });
				});
			});
		});
	}
	
	Projections.prototype.getAllAggregates = function() {
		var base = this;
		
		return new this.boot.Promise(function (resolve, reject) {
			
			base.pool.getConnection(function(err, connection) {
				if (err) return reject(new Error(err));
					
				var query = base.boot.multiline(function() {
					/*
						select
							a.id as aggId,
							a.created_date
						from
							aggregates a
					*/
				});		

				connection.query(query, [
				
				], function(err, rows, fields) {
					connection.release();
					
					if (err) return reject(new Error(err));
					
					return resolve({ rows: rows });
				});
				
			});
			
		});
	}
	
	return Projections;

})();