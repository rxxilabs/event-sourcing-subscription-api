'use strict';

// http://stackoverflow.com/questions/12737371/calling-already-defined-routes-in-other-routes-in-express-nodejs

module.exports = function( app, boot ) {
	
	var ProjectionQueryHandlers = boot.queryHandlers.Projections(app, boot);
	
	app.get('/projections/rebuild', ProjectionQueryHandlers.Rebuild);
	
	app.get('/projections/users', function(req, res) {
		var subs = boot.db.lokidb.getCollection('subscribers');
		res.status(200).json(subs.find());
	});
	
	app.get('/projections/users/:id', function(req, res) {
		var subs = boot.db.lokidb.getCollection('subscribers');
		var user = subs.find({ id: req.params.id });
		res.status(200).json(user);
	});
}