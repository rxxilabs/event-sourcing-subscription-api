'use strict';

module.exports = function( app, boot ) {
	
	return {
		Rebuild: function(req, res) {
		
			var ProjectionsRepo = new boot.repos.Projections(boot);
		
			// 1. Get All Events from MySQL
			// 2. Get All Aggregates
			// 3. Take for the first aggregateId
			// 4. Get all events for this aggregateId in order
			// 5. Apply events to domain for this aggregateId
			// 6. Save state of aggregate in loki event store
			// 7. Go to next aggregate (then proceed from Step 4 again)
			
			var events;
			var subs = boot.db.lokidb.getCollection('subscribers');
		
			ProjectionsRepo.getAllEvents()
				.then(function(data) {
					events = data.rows;
					return ProjectionsRepo.getAllAggregates();
					
				})
				.then(function(data) {
					return data.rows;
				})
				.map(function(aggregate) {
					var aggregateEvents = boot._.filter(events, { 'aggId': aggregate.aggId });
					var Subscriber = new boot.domains.Subscriber(boot);
					
					//console.log("before", Subscriber);
					
					for (var i in aggregateEvents) {
						var eventname = aggregateEvents[i].event;
						var Event = new boot.events[eventname](aggregateEvents[i]);
						Subscriber.Handle(Event);
					}
					
					var results = subs.find({id: Subscriber.id});
					
					//console.log("take out", results);
					
					if (results.length > 0) { 
						console.log("asdasd");
						subs.remove(results);
						console.log("no");
					}
					
 					subs.insert(Subscriber);
					
					//console.log("after", Subscriber);
					
					return aggregate;
				})
				.then(function(result) {
					//console.log("results DB", subs.find());
					if (typeof res !== 'undefined') 
						return res.send('Barf');
				})
				.catch(function (err) {
					if (typeof res !== 'undefined') 
						return res.status(500).json({ status: 'fail', message: 'Error' + err.message, error: err.message });
					else
						throw new Error(err.message);
				});
		
		}
	}
	
}