'use strict';

var fs 		= require('fs');
var express = require('express');
var morgan 	= require('morgan');
var compress = require('compression');
var bodyParser = require('body-parser');
var app;

var loki = require('lokijs');
var mysql 	  = require('./util/mysql');

var boot = require('./bootstrap');

app = module.exports = express();

// create a write stream (in append mode)

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(morgan('combined'));
app.use(compress());
app.use(bodyParser.json());

var env = process.env.NODE_ENV || 'development';
if ('development' == env) {
   // configure stuff here
}

app.get('/', function (req, res) {
	res.send('Hello World!');
});

// http://stackoverflow.com/questions/9699796/sharing-objects-and-avoiding-globals-in-node-js

/** Adding dbs to boot **/
app.use(function(req, res, next) {
	boot.db = {
		lokidb: app.get('lokidb'),
		mysql: app.get('mysqldb'),
	}
	next();
});

require('./controllers/index')(app, boot );
require('./controllers/projections')(app, boot );

app.on('event:app/started', function () {
	
	// Setup databases
	var Mysql = new mysql();
    app.set('mysqldb', Mysql);
	
	var lokidb = new loki('db/db.json');
	var subs = lokidb.addCollection('subscribers', { indices: ['id']});
	subs.ensureUniqueIndex('id');
	
	boot.db = {
		mysql: Mysql,
		lokidb: lokidb
	}
	
	var ProjectionQueryHandlers = boot.queryHandlers.Projections(app, boot);
	ProjectionQueryHandlers.Rebuild();
	
	app.set('lokidb', lokidb);

});