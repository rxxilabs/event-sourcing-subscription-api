'use strict';

var app = require('./index.js');
var http = require('http');

var server;

server = http.createServer(app);

server.listen(process.env.PORT || 8100);
server.on('listening', function () {
	app.emit('event:app/started');
    console.log('Server listening on http://localhost:%d', this.address().port);
});